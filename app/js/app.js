$(function () {
	'use strict';

	// Flickity galleryes
	$('[data-attach="gallery"]').flickity({
		draggable: false,
		pageDots: false
	});

	$('[data-attach="fullsize-gallery"]').flickity({
		autoplay: 8000,
		prevNextButtons: false,
		pageDots: false
	});

	// Slider for catalog filter
	if ($('.slider').length) {
		var $slider = $('.slider');
		var $fromInput = $slider.next('.input-group').find('.form-control.from-input');
		var $toInput = $slider.next('.input-group').find('.form-control.to-input');

		$slider.slider({
			range: true,
			min: 0,
			step: 5000,
			max: 350000,
			values: [
				$fromInput.val(),
				$toInput.val()
			],
			slide: function (event, ui) {
				$fromInput.val(ui.values[0]);
				$toInput.val(ui.values[1]);
			}
		});
	}

	// Mobile menu handler
	$(document).on('click', '.mobile-menu-toggle', function (event) {
		event.preventDefault();
		var mobileNavbar = $('.mobile-navbar');
		if ($(this).hasClass('active')) {
			$(this).removeClass('active');
			mobileNavbar.collapse('hide');
		} else {
			$(this).addClass('active');
			mobileNavbar.collapse('show');
		}
	});

	// Mobile catalog item collapsible content
	$('.catalog-item').on('click', '.js-mobile-collapse-content-handler', function (event) {
		event.preventDefault();
		var collapsingElem = $(this).closest('.item-header').find('.collapse');
		if ($(this).hasClass('collapsed')) {
			collapsingElem.collapse('show');
			$(this).removeClass('collapsed');
		} else {
			collapsingElem.collapse('hide');
			$(this).addClass('collapsed');
		}
	});

	// Catalog filters collapsing
	$('.catalog-filter').on('click', '.block-header', function () {
		var collapsingElem = $(this).next('.block-body');
		if ($(this).hasClass('collapsed')) {
			collapsingElem.collapse('show');
			$(this).removeClass('collapsed');
		} else {
			collapsingElem.collapse('hide');
			$(this).addClass('collapsed');
		}
	}).on('click', '.js-category-collapse-handler', function (event) {
		event.preventDefault();
		var collapsingElem = $(this).next('ul');
		if ($(this).hasClass('collapsed')) {
			collapsingElem.collapse('show');
			$(this).removeClass('collapsed');
		} else {
			collapsingElem.collapse('hide');
			$(this).addClass('collapsed');
		}
	});

	// Mobile catalog filter
	$('.filters-col, .results-col').on('click', '.js-mobile-filters-handler', function (event) {
		event.preventDefault();
		$(this).closest('.row').find('.filters-col').toggleClass('visible');
	});
});
